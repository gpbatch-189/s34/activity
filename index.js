
const express = require('express')
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// 1. Create a GET route that will access the /home route that will print out a simple message.
//Process a GET request at the /home route using postman.

app.get('/home', (request, response) => {
	response.send ('Welcome to the home page')
})





// // Post request route
// app.post('/hello', (request, response) => {
// 	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
// })


// 3. Create a GET route that will access the / users route that will retrieve all the users in the mock database.
// 4. Process a GET request at the /users route using postman.
//

let users = []
app.get('/users', (request, response) => {
	response.send (request.body)
})

// 5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database.
// 6. Process a DELETE request at the /delete-user route using postman.


app.delete('/delete-user', (request, response) => {
	if(request.body.username == request.body.username)
	response.send (`User ${username} has been deleted.`)
})


// Register user route

// let users = []

// app.post('/register', (request, response) => {
// 	console.log(request.body)

// 	if(request.body.username !== '' && request.body.password !== ''){
// 		users.push(request.body)
// 		console.log(users)
// 		response.send(`User ${request.body.username} successfully registered`)
// 	}	else {
// 		response.send(`Please input BOTH username and password.`)
// 	}
// })

// app.put('/change-password', (request, response) => {

// 	// Loop through the whole users array (declared above) so that the server can check if the username that was inputted from the request matches any of the existing usernames inside that array

// 	let message
// 	for(let i = 0; i < users.length; i++){
		
// 		if(request.body.username == users[i].username){ //If a username matches, re-assign/change the existing user's password
// 			users[i].password = request.body.password

// 			message = `User ${request.body.username}'s password has been updated!`

// 			break

// 		} else { // If no username matches, set the message variable to a string saying that the user does not exist.
			
// 			message = 'User does not exist.'
// 		}
// 	}
// 	// Send a response with the respective message depending on the result of the condition
	
// 	response.send(message)
// })


app.listen(port, () => console.log(`Server is running at port ${port}`))
